#include <cassert>
#include <iostream>
#include <vector>
#include <deque>

#include <utility>
// using rel_ops::operator!=;
/*
 * You must implement a unique handle that stores a pointer
 * You may not use any stl classes or functions
 * test1 - 10 points
 * test2 - 10 points
 * test3 - 10 points
 * test4 - 10 points
 */
#define TEST1
#define TEST2
#define TEST3
#define TEST4

// template <typename T>
// class unique_handle{
//
// 	friend bool operator == (const unique_handle& lhs, const unique_handle& rhs){
// 		lhs._p == rhs._p;
// 	}
//
// 	friend bool operator != (const unique_handle& lhs, const unique_handle& rhs){
// 		!(lhs._p == rhs._p);
// 	}
//
// private:
// 	T* _p;
// public:
// 		unique_handle(){
// 			_p = new T();
// 		}
//
// 		unique_handle(T* p){
// 			_p = p;
// 		}
//
//
// 		unique_handle(const unique_handle& rhs){
// 			_p = new T();
// 			*_p = *(rhs._p);
// 		}
//
// 		unique_handle(unique_handle&& rhs){
// 			swap(rhs);
// 		}
//
// 		~unique_handle() = default;
//
// 		unique_handle& operator = (T* p){
// 			_p = p;
// 			return *this;
// 		}
//
//
// 		unique_handle& operator = (const unique_handle& rhs){
// 			if (this == &rhs){
// 					return *this;
// 			}
// 			unique_handle that(rhs);
// 			swap(that);
// 			return *this;
// 		}
//
// 		unique_handle& operator = (unique_handle&& rhs){
// 			if (this == &rhs){
// 				return *this;
// 			}
// 			unique_handle that(move(rhs));
// 			swap(that);
// 			return *this;
// 		}
//
//
// 		T& operator * (){
// 			return *_p;
// 		}
//
// 		// const T& operator *() const {
// 		// 	return *(const_cast<unique_handle*>(this))->_p;
// 		// }
//
// 		void swap(unique_handle& rhs){
// 			std::swap(_p, rhs._p);
// 		}
// };

//
template<typename T>
class unique_handle
{
    friend bool operator == (const unique_handle& lhs, const unique_handle& rhs)
    {
        return lhs._p == rhs._p;
    }

    friend bool operator != (const unique_handle& lhs, const unique_handle& rhs)
    {
        return !(lhs._p == rhs._p);
    }
    private:
        T* _p;

    public:
        unique_handle()
        {
            _p = new T();
        }

        unique_handle(T* p)
        {
            _p = p;
        }

        unique_handle(const unique_handle& rhs)
        {
            _p = new T();
            *_p = *(rhs._p);
        }

        unique_handle(unique_handle&& rhs)
        {
            swap(rhs);
        }

        ~unique_handle() = default;

        T& operator * ()
        {
            return *_p;
        }

        unique_handle& operator = (T* p)
        {
            _p = p;
            return *this;
        }

        unique_handle& operator = (const unique_handle& rhs)
        {
            if(this == &rhs)
                return *this;
            unique_handle that(rhs);
            swap(that);
            return *this;
        }

        unique_handle& operator = (unique_handle&& rhs)
        {
            if(this == &rhs)
                return *this;
            unique_handle that(move(rhs));
            swap(that);
            return *this;
        }

        void swap(unique_handle& rhs)
        {
            std::swap(_p, rhs._p);
        }
};

void test1() {
#ifdef TEST1
	unique_handle<int> y(new int);
	*y = 0;
	assert(*y == 0);
#endif
}

void test2() {
#ifdef TEST2
	unique_handle<int> y;
	y = new int;
	*y = 3;
	assert(*y == 3);
	unique_handle<int> z(new int);
	*z = 3;
	assert(z != y);
	assert(*z == *y);
#endif
}

void test3() {
#ifdef TEST3
	unique_handle<int> y(new int);
	 *y = 3;
	unique_handle<int> z = y;
	assert(z != y);
	assert(*z == *y);
#endif
}

void test4() {
#ifdef TEST4
	unique_handle<int> y(new int);
	*y = 3;
	int *p = &*y;
	unique_handle<int> z = std::move(y);
	assert(*z = 3);
	assert(&*z == p);
#endif
}



int main() {
	int a;
	std::cin >> a;
	switch (a) {
		case 1:
			test1();
			break;
		case 2:
			test2();
			break;
		case 3:
			test3();
			break;
		case 4:
			test4();
			break;
		default:
			break;
	}
	return 0;
}
